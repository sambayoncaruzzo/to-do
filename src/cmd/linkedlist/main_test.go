package cmd_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/sambayoncaruzzo/to-do/src/cmd"
)

func TestCreateRootNode(t *testing.T) {
	node := cmd.NewRootNode("author", "title")
	expectedAuthor := "author"
	expectedTitle := "title"

	assert.Equal(t, expectedAuthor, node.GetAuthor())
	assert.Equal(t, expectedTitle, node.GetTitle())
}

func TestCreatedNode(t *testing.T) {
	author := "ale"
	rootNode := cmd.NewRootNode(author, "root")
	node, err := cmd.NewNode(author, "node", &rootNode)
	if err != nil {
		assert.Fail(t, "")
	}

	assert.Equal(t, node.GetParent(), &rootNode)
}
