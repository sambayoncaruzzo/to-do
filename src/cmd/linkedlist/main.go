package cmd

import "errors"

type node struct {
	parent *node
	author string
	title  string
}

func NewRootNode(author_ string, title_ string) node {
	return newNode(author_, title_, nil)
}

func NewNode(author_ string, title_ string, parent_ *node) (node, error) {
	if parent_ == nil {
		return node{}, errors.New("Cannot create a child node without a parent node")
	}

	return newNode(author_, title_, parent_), nil
}

func newNode(author_ string, title_ string, parent_ *node) node {
	var node = node{
		author: author_,
		title:  title_,
		parent: parent_,
	}

	return node
}

func (n *node) GetAuthor() string {
	return n.author
}

func (n *node) GetTitle() string {
	return n.title
}

func (n *node) GetParent() *node {
	return n.parent
}
