package commands

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "todo",
	Short: "To planify, manage tasks and routines",
	Long:  "A long description",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Printf("Run cmd: '%v', and args: '%v'", cmd, args)

	},
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintf(os.Stderr, "Whoops. There was an error while executing todo cli: '%s'", err)
		os.Exit(1)
	}
}
