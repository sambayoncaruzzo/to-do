package main

import (
	"gitlab.com/sambayoncaruzzo/to-do/src/cmd/cli/commands"
)

func main() {
	commands.Execute()
}
