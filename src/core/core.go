package core

import "github.com/google/uuid"

type CategoryRepository interface {
	ListCategories() []Category
}

type CategoryService struct {
	categoryRepository CategoryRepository
}

func NewCategoryService(_cr CategoryRepository) *CategoryService {
	return &CategoryService{
		categoryRepository: _cr,
	}
}

type Category struct {
	id    uuid.UUID
	title string
	tasks []Task
}

type Task struct {
	id          uuid.UUID
	title       string
	description string
}

func (cs *CategoryService) CreateCategory(_title string) Category {
	return Category{
		id:    uuid.New(),
		title: _title,
		tasks: make([]Task, 0),
	}
}

func (cs *CategoryService) ListCategories() []Category {
	return cs.categoryRepository.ListCategories()
}

func createTask(_title string, _description string) Task {
	return Task{
		id:          uuid.New(),
		title:       _title,
		description: _description,
	}
}

func (c *Category) AddTask(_title string, _description string) {
	c.tasks = append(c.tasks, createTask(_title, _description))
}

func (c *Category) GetTasks() []Task {
	return c.tasks
}

func (c *Category) GetTitle() string {
	return c.title
}

func (t *Task) GetTitle() string {
	return t.title
}

func (t *Task) GetDescription() string {
	return t.description
}
