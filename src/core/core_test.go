package core_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/sambayoncaruzzo/to-do/src/core"
)

func TestCreateTask(t *testing.T) {
	categoryService := core.NewCategoryService(nil)
	category := categoryService.CreateCategory("comidas")
	category.AddTask("a title", "a description")

	returnedTasks := category.GetTasks()

	assert.Equal(t, "comidas", category.GetTitle())
	assert.Equal(t, 1, len(returnedTasks))
	assert.Equal(t, "a title", returnedTasks[0].GetTitle())
	assert.Equal(t, "a description", returnedTasks[0].GetDescription())
}

func TestListCategories_EmptyCase(t *testing.T) {
	categoryRepository := mockCategoryRepository{categories: make([]core.Category, 0)}
	categoryService := core.NewCategoryService(categoryRepository)

	categories := categoryService.ListCategories()

	assert.Len(t, categories, 0)
}

func TestListCategories_OneCategory(t *testing.T) {
	category := core.Category{}
	categories := []core.Category{category}
	categoryRepository := mockCategoryRepository{categories: categories}
	categoryService := core.NewCategoryService(categoryRepository)

	result_categories := categoryService.ListCategories()

	assert.Len(t, categories, 1)
	assert.EqualValues(t, category, result_categories[0])
}

func TestListCategories_TwoCategory(t *testing.T) {
	category_one := core.Category{}
	category_two := core.Category{}
	categories := []core.Category{category_one, category_two}
	categoryRepository := mockCategoryRepository{categories: categories}
	categoryService := core.NewCategoryService(categoryRepository)

	result_categories := categoryService.ListCategories()

	assert.Len(t, categories, 1)
	assert.EqualValues(t, category_one, result_categories[0])
	assert.EqualValues(t, category_two, result_categories[1])
}

type mockCategoryRepository struct {
	categories []core.Category
}

func (cr mockCategoryRepository) ListCategories() []core.Category {
	return cr.categories
}
